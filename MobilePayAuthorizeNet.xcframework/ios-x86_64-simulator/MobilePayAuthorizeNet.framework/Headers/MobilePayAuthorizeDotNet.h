//
//  MobilePayAuthorizeDotNet.h
//  MobilePayAuthorizeDotNet
//
//  Created by Marius Kurgonas on 05/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MobilePayAuthorizeDotNet.
FOUNDATION_EXPORT double MobilePayAuthorizeDotNetVersionNumber;

//! Project version string for MobilePayAuthorizeDotNet.
FOUNDATION_EXPORT const unsigned char MobilePayAuthorizeDotNetVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MobilePayAuthorizeDotNet/PublicHeader.h>


