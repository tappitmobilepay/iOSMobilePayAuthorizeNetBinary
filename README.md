# How to install

#### Cocoapods

- Put the following cocoapods specs repo link at the top of Podfile: https://gitlab.com/tappitmobilepay/iosmobilepayauthorizenetspecs
- Add `pod MobilePayAuthorizeNet` to your Podfile under a target app
- Run `pod install`

# How to use

Before using the main MobilePay SDK, initialize this SDK with the AuthorizeNet credentials like so:

`MobilePayAuthorizeNet.setupWith(loginId: loginId, publicClientKey: publicClientKey)`